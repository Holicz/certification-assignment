<?php

declare(strict_types = 1);

namespace App\Tests\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends WebTestCase
{
    public function testAccessDeniedForAnonymousUsers(): void
    {
        $urls = [
            '/todos',
            '/admin',
            '/logout'
        ];

        foreach ($urls as $url) {
            $this->isAccessDeniedForAnonymousUsers($url);
        }
    }

    public function testAccessAllowedForAnonymousUsers(): void
    {
        $urls = [
            '/login',
            '/register'
        ];

        foreach ($urls as $url) {
            $this->isAccessAllowedForAnonymousUsers($url);
        }
    }

    public function testCreateUser(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $form = $crawler->selectButton('Register')->form([
            'user[email]' => 'email@emaple.org',
            'user[plainPassword][first]' => 'password',
            'user[plainPassword][second]' => 'password'
        ]);

        $client->submit($form);

        static::assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());

        /** @var User $user */
        $user = $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy([
            'email' => 'email@emaple.org'
        ]);

        static::assertNotNull($user);
        static::assertSame('email@emaple.org', $user->getEmail());
    }

    public function testCreateUserWithExistingAddress(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $form = $crawler->selectButton('Register')->form([
            'user[email]' => 'email@emaple.org',
            'user[plainPassword][first]' => 'password',
            'user[plainPassword][second]' => 'password'
        ]);

        $client->submit($form);

        static::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Remove created user
        $doctrine = $client->getContainer()->get('doctrine');
        $user = $doctrine->getRepository(User::class)->findOneBy([
            'email' => 'email@emaple.org'
        ]);

        $doctrine->getManager()->remove($user);
        $doctrine->getManager()->flush();
    }

    private function isAccessDeniedForAnonymousUsers(string $url): void
    {
        $client = static::createClient();
        $client->request('GET', $url);

        // HTTP_FOUND (302) is redirect to login
        static::assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());
    }

    private function isAccessAllowedForAnonymousUsers(string $url): void
    {
        $client = static::createClient();
        $client->request('GET', $url);

        static::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
