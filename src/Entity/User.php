<?php

declare(strict_types = 1);

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="This email is already in use")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="Email cannot be blank")
     * @Assert\Email(message="Email is not valid")
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     *
     * @var array
     */
    private $roles = [];

    /**
     * @Assert\NotBlank(message="Password cannot be blank")
     * @Assert\Length(max=4096, maxMessage="Password can be maximally 4096 characters long", min="3", minMessage="Password must be at least 3 characters long")
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="Todo", mappedBy="user")
     */
    private $todos;

    public function __construct() {
        $this->todos = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function addRole(string $role): void
    {
        if ($this->hasRole($role)) {
            return;
        }

        $roles = $this->getRoles();
        $roles[] = $role;

        $this->setRoles($roles);
    }

    public function removeRole(string $role): void
    {
        if (!$this->hasRole($role)) {
            return;
        }

        $roles = $this->getRoles();
        /** @var int $key */
        $key = \array_search($role, $roles, true);
        unset($roles[$key]);

        $this->setRoles($roles);
    }

    public function hasRole(string $role): bool
    {
        return \in_array($role, $this->getRoles(), true);
    }

    public function setPlainPassword(string $password): void
    {
        $this->plainPassword = $password;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        // not needed when using the "argon" algorithm in security.yaml
    }

    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
    }

    public function addTodo(Todo $todo): void
    {
        $this->todos->add($todo);
    }

    public function removeTodo(Todo $todo): void
    {
        $this->todos->removeElement($todo);
    }

    public function getTodos(): ArrayCollection
    {
        return $this->todos;
    }
}
