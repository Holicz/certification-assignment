<?php

declare(strict_types = 1);

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function promoteToAdmin(User $user): void
    {
        $user->addRole('ROLE_ADMIN');

        $this->save($user);
    }

    public function demoteFromAdmin(User $user): void
    {
        $user->removeRole('ROLE_ADMIN');

        $this->save($user);
    }

    public function remove(User $user): void
    {
        $this->em->remove($user);
        $this->em->flush();
    }

    private function save(User $user): void
    {
        $this->em->persist($user);
        $this->em->flush();
    }
}
