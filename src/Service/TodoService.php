<?php

declare(strict_types = 1);

namespace App\Service;

use App\Entity\Todo;
use App\Entity\User;
use App\Model\TodoModel;
use Doctrine\ORM\EntityManagerInterface;

class TodoService
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function createFromModel(TodoModel $todoModel, User $user): void
    {
        $todo = new Todo();
        $todo->setUser($user);
        $todo->setTitle($todoModel->title);

        $this->save($todo);
    }

    public function complete(Todo $todo): void
    {
        $todo->setDone(true);

        $this->save($todo);
    }

    public function uncomplete(Todo $todo): void
    {
        $todo->setDone(false);

        $this->save($todo);
    }

    public function remove(Todo $todo): void
    {
        $this->em->remove($todo);
        $this->em->flush();
    }

    private function save(Todo $todo): void
    {
        $this->em->persist($todo);
        $this->em->flush();
    }
}
