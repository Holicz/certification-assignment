<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\User;
use App\Security\UserVoter;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Route("/", name="app_admin_index")
     * @Template
     */
    public function index(): array
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->findAll();

        return [
            'users' => $users
        ];
    }

    /**
     * @Route("/user/remove/{id}", name="app_admin_user_remove")
     */
    public function remove(User $user): RedirectResponse
    {
        $this->denyAccessUnlessGranted(UserVoter::REMOVE, $user);

        $this->userService->remove($user);
        $this->addFlash('warning', 'User was removed');

        return $this->redirectToRoute('app_admin_index');
    }

    /**
     * @Route("/user/promote/{id}", name="app_admin_user_promote")
     */
    public function promote(User $user): RedirectResponse
    {
        $this->denyAccessUnlessGranted(UserVoter::CHANGE_ROLE, $user);

        $this->userService->promoteToAdmin($user);
        $this->addFlash('success', 'User was made an admin');

        return $this->redirectToRoute('app_admin_index');
    }

    /**
     * @Route("/user/demote/{id}", name="app_admin_user_demote")
     */
    public function demote(User $user): RedirectResponse
    {
        $this->denyAccessUnlessGranted(UserVoter::CHANGE_ROLE, $user);

        $this->userService->demoteFromAdmin($user);
        $this->addFlash('warning', 'User is no longer an admin');

        return $this->redirectToRoute('app_admin_index');
    }
}
