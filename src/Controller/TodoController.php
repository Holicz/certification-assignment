<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Todo;
use App\Form\TodoType;
use App\Model\TodoModel;
use App\Security\TodoVoter;
use App\Service\TodoService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/todos")
 */
class TodoController extends AbstractController
{
    private $todoService;

    public function __construct(TodoService $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * @Route("/", name="app_todo_index")
     * @Template
     *
     * @return array|RedirectResponse
     */
    public function index(Request $request)
    {
        // Get all todos for user
        $user = $this->getUser();

        $todoRepository = $this->getDoctrine()->getRepository(Todo::class);
        $todos = $todoRepository->findAllByUser($user);

        // Create form for new task at the end of the list
        $todoModel = new TodoModel();
        $form = $this->createForm(TodoType::class, $todoModel);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->todoService->createFromModel($todoModel, $user);

            $this->addFlash('success', 'Todo was created');

            return $this->redirectToRoute('app_todo_index');
        }

        return [
            'todos' => $todos,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/complete/{id}", name="app_todo_complete")
     */
    public function complete(Todo $todo): RedirectResponse
    {
        $this->denyAccessUnlessGranted(TodoVoter::CHANGE_DONE, $todo);

        $this->todoService->complete($todo);
        $this->addFlash('success', 'Todo was completed');

        return $this->redirectToRoute('app_todo_index');
    }

    /**
     * @Route("/uncomplete/{id}", name="app_todo_uncomplete")
     */
    public function uncomplete(Todo $todo): RedirectResponse
    {
        $this->denyAccessUnlessGranted(TodoVoter::CHANGE_DONE, $todo);

        $this->todoService->uncomplete($todo);
        $this->addFlash('success', 'Todo was uncompleted');

        return $this->redirectToRoute('app_todo_index');
    }

    /**
     * @Route("/remove/{id}", name="app_todo_remove")
     */
    public function remove(Todo $todo): RedirectResponse
    {
        $this->denyAccessUnlessGranted(TodoVoter::REMOVE, $todo);

        $this->todoService->remove($todo);
        $this->addFlash('warning', 'Todo was removed');

        return $this->redirectToRoute('app_todo_index');
    }
}
