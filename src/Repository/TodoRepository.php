<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class TodoRepository extends EntityRepository
{
    public function findAllByUser(User $user)
    {
        return $this->createQueryBuilder('t')
            ->where('t.user = :user')
            ->orderBy('t.done', 'DESC')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
