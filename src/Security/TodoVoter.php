<?php

declare(strict_types = 1);

namespace App\Security;

use App\Entity\Todo;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TodoVoter extends Voter
{
    public const REMOVE = 'remove';
    public const CHANGE_DONE = 'change_done';

    protected function supports($attribute, $subject): bool
    {
        // if the attribute isn't one we support, return false
        if (!\in_array($attribute, [self::REMOVE, self::CHANGE_DONE], true)) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Todo) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        switch ($attribute) {
            case self::REMOVE:
                return $this->canRemove($subject, $user);
            case self::CHANGE_DONE:
                return $this->canChangeDone($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canRemove(Todo $subject, User $user): bool
    {
        // User can remove only his tasks
        return $subject->getUser() === $user;
    }

    private function canChangeDone(Todo $subject, User $user): bool
    {
        // User can change only his tasks
        return $subject->getUser() === $user;
    }
}
