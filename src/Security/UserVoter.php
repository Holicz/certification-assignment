<?php

declare(strict_types = 1);

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{
    public const REMOVE = 'remove';
    public const CHANGE_ROLE = 'change_role';

    protected function supports($attribute, $subject): bool
    {
        // if the attribute isn't one we support, return false
        if (!\in_array($attribute, [self::REMOVE, self::CHANGE_ROLE], true)) {
            return false;
        }

        // only vote on Post objects inside this voter
        /** @var User $subject */
        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        switch ($attribute) {
            case self::REMOVE:
                return $this->canRemove($subject, $user);
            case self::CHANGE_ROLE:
                return $this->canChangeRole($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canRemove(User $subject, User $user): bool
    {
        // Only admin can remove users
        if (!$user->hasRole('ROLE_ADMIN')) {
            return false;
        }

        // User can't delete himself
        if ($user === $subject) {
            return false;
        }

        return true;
    }

    private function canChangeRole(User $subject, User $user): bool
    {
        // Only admin can change user roles
        if (!$user->hasRole('ROLE_ADMIN')) {
            return false;
        }

        // User can't change his role
        if ($user === $subject) {
            return false;
        }

        return true;
    }
}
