<?php

declare(strict_types = 1);

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class TodoModel
{
    /**
     * @Assert\NotBlank(message="Todo cannot be blank.")
     * @Assert\Length(min="3", minMessage="Todo must be at least 3 characters long.", max="35", maxMessage="Todo can be maximally 35 characters long.")
     *
     * @var string
     */
    public $title;
}
