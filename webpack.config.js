var Encore = require('@symfony/webpack-encore');

Encore
    // Set up directory where to output assets
    .setOutputPath('public/build/')
    .setPublicPath('/build')

    // Add our assets
    .addEntry('app', './assets/scss/app.scss')
    .addEntry('login', './assets/scss/login.scss')
    .addEntry('registration', './assets/scss/registration.scss')

    // Turn on SASS
    .enableSassLoader()

    // Encore settings
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
;

module.exports = Encore.getWebpackConfig();
